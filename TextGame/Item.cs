﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextGame
{
    public abstract class Item
    {
        public abstract string Name { get; set; }

        public abstract void CollectItem();
    }
}
