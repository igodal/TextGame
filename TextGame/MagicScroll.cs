﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextGame
{
    class MagicScroll : Item
    {
        public override string Name { get; set; }

        public override void CollectItem()
        {
            Name = "Misterious Scroll";
            Console.WriteLine($"\nYou've added a strange {Name} to your bag.");
        }

        // public void UseItem()
        // {
        //     Console.WriteLine("");
        // }
    }
}
