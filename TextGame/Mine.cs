﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace TextGame
{
    public class Mine : IBag
    {
        private List<String> itemList = new List<String> ();
        private Item[] itemsCollected = new Item[0];
        
        private static Random random = new Random();

        public void EnterMine()
        {
            Console.WriteLine("Greetings, adventurer. You've reached the Dark Mine. Are you brave enough to look for the treasure?" +
                              "\n1 - Enter the Mine." +
                              "\n2 - Run away.");
            char choice = Console.ReadKey().KeyChar;
            if (choice == '1')
            {
                Console.WriteLine("\nGood luck. You're gonna need this.");
            }
            else if (choice == '2')
            {
                Console.WriteLine("\nMwahaha, run for your life!");
            }
            else
            {
                Console.WriteLine("\nYou noticed the sound above you, but it's too late to dodge this huge rock...");
            }
        }

        public bool SearchForItem()
        {
            int didFindItem = random.Next(0, 2);
            if (didFindItem == 1)
            {
                Console.WriteLine("You've found a chest with something inside..." +
                                  "\nDo you want to pick it up?" +
                                  "\n1 - pick the item" +
                                  "\nAnything else - don't pick it");
                char pickItem = Console.ReadKey().KeyChar;
                if (pickItem == '1')
                {
                    AddItem();
                }
                else
                {
                    Console.WriteLine("No? Your loss...");
                }

                return true;
            }

            else
            {
                Console.WriteLine("This area is empty.");
                return false;
            }

            
        }

        public bool GoDeeper()
        {
            Console.WriteLine("\nDo you want to go deeper?" +
                              "\n0 - return" +
                              "\n1 - go deeper" +
                              "\n2 - stay awhile and check your items.");
            char isGoingDeeper = Console.ReadKey().KeyChar;
            if (isGoingDeeper == '1')
            {
                Console.WriteLine("\nYou're going deeper. Perhaps you wil find something here...");
                SearchForItem();
                return true;
            }
            else if (isGoingDeeper == '0')
            {
                Console.WriteLine("\nYou're going back. Farewell");
                return false;

            }else if (isGoingDeeper == '2')
            {
                Console.WriteLine("\nResting.");
                ShowItems();
                return true;

            }
            else
            {
                Console.WriteLine("\nYou have only two choises. Try again.");
                return true;
            }
        }

        public void AddItem()
        {
            int foundItem = random.Next(1, 3);
            switch (foundItem)
            {
                case 1:
                    Item coin = new Coin();
                    coin.CollectItem();
                    itemList.Add(coin.Name);
                    break;
                case 2:
                    Item magicScroll = new MagicScroll();
                    magicScroll.CollectItem();
                    itemList.Add(magicScroll.Name);
                    break;
                // case 3:
                //     Item map = new Map();
                //     map.CollectItem();
                //     itemList.Add(map);
                //     break;
            }

            
            Console.WriteLine("\n*Putting the item to bag*");
        }

        public void ShowItems()
        {
            foreach (var i in itemList)
            {
                Console.WriteLine(i);
            }

        }

    }
}
