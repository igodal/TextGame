﻿using System;

namespace TextGame
{
    public class Program
    {
        static void Main(string[] args)
        {
            Item[] itemsCollected = new Item[0];
            Mine mine = new Mine();

            bool a = true;
            mine.EnterMine();
            while (a)
            {
                if (mine.GoDeeper())
                {
                    a =  true;
                }
                else
                {
                    a = false;
                }

            }
        }
    }
}
