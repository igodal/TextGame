﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TextGame
{
    class Map : Item
    {
        public override string Name { get; set; }
        private const int ALL_MAP_FRAGMENTS = 4;
        private int fragmentCount { get; set; }

        public override void CollectItem()
        {
            fragmentCount++;
            Name = "Map fragment";
            Console.WriteLine($"You've found a {Name}.");
            if (fragmentCount == 1)
            {
                Console.WriteLine("Ah, a fragment of a map. I have to look for another.");
            }
            else if (fragmentCount > 0 && fragmentCount < 3)
            {
                Console.WriteLine("Another one! Few more and I'll have a map to the treasure...");
            }
            else if (fragmentCount == 3)
            {
                Console.WriteLine($"WOO-HOO! I have the whole map!!! Treasure is mine!");
            }
        }
    }
}
