﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;

namespace TextGame
{
    class Coin : Item
    {
        public override string Name { get; set; }

        public override void CollectItem()
        {
            Name = "Golden Coin";
            Console.WriteLine($"You've added {Name} to your collection. But be careful! You never know who's looking for it.");
        }
    }
}
